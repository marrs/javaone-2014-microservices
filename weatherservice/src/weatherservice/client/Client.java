package weatherservice.client;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import weatherservice.api.WeatherService;

public class Client {
	private static final List<String> CITIES = Arrays.asList(
		"Amsterdam", "Berlin", "Copenhagen", 
		"Delft", "Eindhoven", "Frankfurt", 
		"Groningen", "Harderwijk", "Ingolstadt", 
		"Jerusalem", "Kampen", "Lisbon",
		"Madrid", "Noordwijk aan Zee", "Oslo",
		"Paris", "Rotterdam", "Scheveningen",
		"Tel-Aviv", "Utrecht", "Vienna",
		"Woensdrecht", "X-City", "Y-Town",
		"Zwolle"
	);

	private volatile WeatherService m_weatherService;

	public double city(String city) {
		System.out.println("Getting weather for " + city + ":");
		double temperature = m_weatherService.predictTemperature(city);
		return temperature;
	}
	
	public void cities() throws InterruptedException {
		ExecutorService pool = Executors.newFixedThreadPool(5);
		for (String city : CITIES) {
			pool.submit(() -> { System.out.println("The temperature in " + city + " is " + m_weatherService.predictTemperature(city)); });
		}
		pool.shutdown();
		pool.awaitTermination(60, TimeUnit.SECONDS);
	}
}
